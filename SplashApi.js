import * as axios from 'axios';

const host_address = '47.92.141.19' //'127.0.0.1'
const port = 9981//5000

function getTransType(accuracy) {
    var transType = 1;
    console.log(`accuracy: ${accuracy}`);
    switch (accuracy) { 
        case 550:
            transType = 3;
            break;
        case 30:
            transType = 1;
            break;
        default:
            transType = 1;
            break;
    }
    return transType;
}

export function get_current_location() {
    return new Promise((resolve, reject) => {
        var geolocation = navigator.geolocation;
        geolocation.getCurrentPosition((location) => {
            let latitude = location.coords.latitude;
            let longitude = location.coords.longitude;
            let coords = `${longitude},${latitude}`;
            let transType = getTransType(location.coords.accuracy);
            let url = `http://${host_address}:${port}/coordinates_convert?coordinates=${coords}&transType=${transType}`;
            axios.default.get(url)
                .then(response => response.data)
                .then(data => {
                    var coordsTransferred = '';
                    var parts = data.split(',');
                    if (parts.length >= 2) {
                        var longitude = parts[0];
                        var latitude = parts[1];
                        coordsTransferred = `${latitude},${longitude}`;
                    }
                    else { 
                        reject('get wrong coordinates');
                    }
                    resolve(coordsTransferred);
                })
                .catch(error => {
                    reject(error);
                });
        }, error => console.log(error));
    });
}