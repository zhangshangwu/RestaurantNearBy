import React from 'react';
import { StyleSheet, Text, View, Button, BackHandler, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchRestaurants, pickRestaurant } from './RestaurantActions';

class Home extends React.Component {

    Title = "Hello";

    render() {
        let text;
        if (this.props.restaurants != null) {
            if (this.props.restaurants.error) { 
                text = this._showText('Error happened');
            }
            else if (this.props.restaurants.candidates != null
                && this.props.restaurants.candidates.length > 0) {
                let randomNumber = Math.round(Math.random() * 20);
                let selected = this.props.restaurants.candidates[randomNumber];
                text = this._onSelectedRestaurant(selected);
                
            }
            else {
                text = this._showText('No restaurants selected');
            }
        }
        
        return (
            <View style={styles.container}>
                {
                    !this.props.restaurants.isFetching && <Button
                        title="Click Me"
                        onPress={() =>
                            this.props.fetchRestaurants(this.props.location.location, '10,35', 20)
                        } />
                }
                <View>
                    {
                        this.props.restaurants.isFetching && <Text>Loading...</Text>
                    }
                </View>
                {text}
            </View>
        );
    }

    _showText(value) {
        let text =
            <View>
                <Text>{value}</Text>
            </View>;
        return text;
    }

    _onSelectedRestaurant(selected) {
        let text;
        if (selected !== undefined) {
            text =
                <View>
                    <Text>{selected.name}</Text>
                </View>;
        }
        else {
            text =
                <View>
                    <Text>Selected Failed</Text>
                </View>;
        }
        return text;
    }

    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
                cancelable: false
            }
        )
        return true;
    }
       
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
       
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = (state) => {
    const { location, restaurants } = state;
    return { location, restaurants };
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        fetchRestaurants,
        pickRestaurant
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
