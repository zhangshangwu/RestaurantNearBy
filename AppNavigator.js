import { createStackNavigator } from 'react-navigation';
import Home from './Home';
import Splash from './Splash'

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Splash: Splash
  },
  {
    initialRouteName: 'Splash',
  });

export default AppNavigator;
