import { get_restaurants, get_current_location } from './RestaurantApi';
import { FETCH_RESTAURANTS, FETCH_LOCATION } from './RestaurantConsts';

export const fetchRestaurants = (location, price_section, result_count) => ({
    type: FETCH_RESTAURANTS,
    payload: get_restaurants(location, price_section, result_count)
});